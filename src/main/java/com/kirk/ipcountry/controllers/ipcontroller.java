package com.kirk.ipcountry.controllers;
import com.kirk.ipcountry.model.IpLocationModel;
import com.kirk.ipcountry.model.RespEntity;
import com.kirk.ipcountry.servier.IpDataFileServer;
import com.kirk.ipcountry.servier.IpDatabaseServer;
import com.kirk.ipcountry.servier.IpServer;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Getter
@Setter
public class ipcontroller {

    @Autowired
//    @Resource
    private IpDatabaseServer ipDatabaseServer;
//    @Autowired
//    private IpDataFileServer ipDataFileServer;

    @RequestMapping("/")
    public RespEntity ipcheck(@RequestParam(name = "ip",required = false,defaultValue = "0") String ip){
        System.out.printf("\n ## \n");
        System.out.printf(ip);
        System.out.printf("\n ## \n");
        String item[] = ip.split("\\.");
        if (item.length != 4){
            return new RespEntity(-1,"Request format error");
        }
//        if(this.fileServer==null){
//            this.fileServer = new IpDataFileServer();
//        }
        IpLocationModel locationModel = ipDatabaseServer.searchIPInfo(ip);
//        IpLocationModel locationModel = ipDataFileServer.searchIPInfo(ip);
        return new RespEntity(0,locationModel!=null?locationModel:"not found");
    }
}
