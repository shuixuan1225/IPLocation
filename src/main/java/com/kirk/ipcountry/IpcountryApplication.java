package com.kirk.ipcountry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableCaching
@SpringBootApplication
@EnableAsync(proxyTargetClass = true)
@ServletComponentScan
@Configuration
public class IpcountryApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpcountryApplication.class, args);
	}
}
