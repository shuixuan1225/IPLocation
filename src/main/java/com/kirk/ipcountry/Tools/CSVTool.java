package com.kirk.ipcountry.Tools;

import com.kirk.ipcountry.model.IpLocationModel;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CSVTool {

    public ArrayList<IpLocationModel> readFromFile(){
        try {
            Resource resource = new ClassPathResource("cvsfiles/IP2LOCATION-LITE-DB1.CSV");
            File file = resource.getFile();
            BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.readLine();

            ArrayList<IpLocationModel> list = new ArrayList<IpLocationModel>();

            String line = null;
            while ((line = reader.readLine()) != null){
                line = line.replace("\"","");
                String item[] = line.split(",");
                if (item.length < 4 ){
                    return list;
                }else{
                    list.add(new IpLocationModel(Long.parseLong(item[0]), Long.parseLong(item[1]), item[2], item[3]));
                }
            }
            return list;

        } catch (Exception e){
            System.out.printf(e.getLocalizedMessage());
        }

        return null;
    }

    public IpLocationModel searchIPInfo(long ipLong) throws Exception{
        Resource resource = new ClassPathResource("cvsfiles/IP2LOCATION-LITE-DB1.CSV");
        File file = resource.getFile();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        reader.readLine();

        String line = null;
        while ((line = reader.readLine()) != null){
            line = line.replace("\"","");
            String item[] = line.split(",");
            if (item.length < 4 ){
                continue;
            }else{
                IpLocationModel model = new IpLocationModel(Long.parseLong(item[0]), Long.parseLong(item[1]), item[2], item[3]);
                if (model.containsOfIp(ipLong)){
                    return model;
                }
            }
        }
        return null;
    }
}
