package com.kirk.ipcountry.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class IpLocationModel implements Serializable {
    private Long ip_from;
    private Long ip_to;
    private String country_code;
    private String country_name;

    public IpLocationModel(Long sip,Long eip,String ccode ,String country){
        this.setIp_from(sip);
        this.setIp_to(eip);
        this.setCountry_code(ccode);
        this.setCountry_name(country);
    }

    public boolean containsOfIp(long ipLong){
//        System.out.printf("\n %d ## %d ## %d \n",intIp,startip,endip);
        if (ipLong>= this.ip_from && ipLong <= this.ip_to){
            return true;
        }
        return false;
    }
}
