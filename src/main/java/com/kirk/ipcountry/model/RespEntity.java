package com.kirk.ipcountry.model;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class RespEntity implements Serializable {

    private int code;

    private Object model;

    public RespEntity(int code,Object model) {
        this.setCode(code);
        this.setModel(model!=null?model:"error");
    }

}
