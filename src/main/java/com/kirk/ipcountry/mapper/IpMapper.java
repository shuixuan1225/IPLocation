package com.kirk.ipcountry.mapper;

import com.kirk.ipcountry.model.IpLocationModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;

@Mapper
public interface IpMapper {

//    public ArrayList<IpLocationModel> selectAllIpData();

    public IpLocationModel searchIPInfo(Long ipLong);

}
