package com.kirk.ipcountry.servier;

import com.kirk.ipcountry.mapper.IpMapper;
import com.kirk.ipcountry.model.IpLocationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("ipDatabaseServer")
public class IpDatabaseServer extends IpServer {

    @Autowired
    private IpMapper ipMapper;

    @Override
    @Cacheable(cacheNames = "ipLocationDatabaseSearchResult")
    public IpLocationModel searchIPInfo(String ipString) {
        long ipLong = this.ipToLong(ipString);
        System.out.printf("\n ## \n");
        System.out.print(ipLong);
        System.out.printf("\n ## \n");
        return ipMapper.searchIPInfo(ipLong);
    }


    /*
    @Override
    @Cacheable(cacheNames = "ipLocationDatabaseResult")
    public IpLocationModel findLocationModelWithIpString(String ipString) {
        ArrayList<IpLocationModel> list = this.ipMapper.selectAllIpData();

        Long ipLong = this.ipToLong(ipString);
        System.out.printf("\n ## \n");
        System.out.print(ipLong);
        System.out.printf("\n ## \n");
        IpLocationModel locationModel = null;
        for (int i = 0; i < list.size(); i++) {
            IpLocationModel model = list.get(i);
            if (model!=null && model.containsOfIp(ipLong)){
                locationModel = model;
                break;
            }
        }
        return locationModel;
    }
    */
}
