package com.kirk.ipcountry.servier;

import com.kirk.ipcountry.model.IpLocationModel;

public interface IpDataServerInterface {

//    public IpLocationModel findLocationModelWithIpString(String ipString);

    public IpLocationModel searchIPInfo(String ipString);

}
