package com.kirk.ipcountry.servier;

import com.kirk.ipcountry.Tools.CSVTool;
import com.kirk.ipcountry.model.IpLocationModel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service("fileService")
@Setter
@Getter
public class IpDataFileServer extends IpServer {

    @Override
    public IpLocationModel searchIPInfo(String ipString){
        try {
            long longIp = this.ipToLong(ipString);
            System.out.printf("\n ## \n");
            System.out.print(longIp);
            System.out.printf("\n ## \n");
            CSVTool tool = new CSVTool();
            return tool.searchIPInfo(longIp);
        } catch (Exception e){
            System.out.printf(e.getLocalizedMessage());
            return null;
        }
    }
    /*
    public ArrayList<IpLocationModel> ipLocationDataList(){
        CSVTool tool = new CSVTool();
        return tool.readFromFile();
    }

    @Override
    @Cacheable(cacheNames = "ipLocationResult")
    public IpLocationModel findLocationModelWithIpString(String ipString){

        ArrayList<IpLocationModel> list = this.ipLocationDataList();

        long longIp = this.ipToLong(ipString);
        System.out.printf("\n ## \n");
        System.out.print(longIp);
        System.out.printf("\n ## \n");
        IpLocationModel locationModel = null;
        for (int i = 0; i < list.size(); i++) {
            IpLocationModel model = list.get(i);
            if (model!=null && model.containsOfIp(longIp)){
                locationModel = model;
                break;
            }
        }
        return locationModel;
    }
    */

}
