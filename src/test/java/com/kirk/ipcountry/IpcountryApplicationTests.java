package com.kirk.ipcountry;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kirk.ipcountry.controllers.ipcontroller;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = IpcountryApplication.class)
//@WebAppConfiguration
public class IpcountryApplicationTests {

	private static final Logger logger = LoggerFactory.getLogger(SpringBootTest.class);

	@Autowired
	WebApplicationContext wac;
	private MockMvc mvc;

	@Before
    public void setUp() throws Exception{
	    mvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

	@Test
	public void test1() throws Exception{
	    logger.debug("测试get请求无参数");
	    mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
	}

    @Test
    public void test2() throws Exception{
        logger.debug("测试get请求带参数......");
        //mock进行模拟
        mvc.perform(MockMvcRequestBuilders.get("/").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).param("ip","172.96.237.83"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }


    @Test
    public void test3() throws Exception{
        logger.debug("测试post请求带参数......");

        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put("ip","172.96.237.83");

        ObjectMapper objectMapper=new ObjectMapper();
        String requestJson=objectMapper.writeValueAsString(hashMap);

        //mock进行模拟
        mvc.perform(MockMvcRequestBuilders.post("/").accept(MediaType.APPLICATION_JSON_UTF8_VALUE).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param("ip","172.96.237.83")
                .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

//    @Test
//    public void contextLoads() throws Exception{
//        logger.debug("测试get请求无参数");
//        String url = "http://localhost:8081/";
////        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
////        map.add("name", "Tom");
//        String result = template.getForObject(url,String.class);//template.postForObject(url, map, String.class);
//        System.out.println(result);
//    }

}
